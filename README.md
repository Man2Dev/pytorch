# Fedora PyTorch
TBD summary.


### Getting Started

Get setup with the [Fedora Packager Tools](https://docs.fedoraproject.org/en-US/package-maintainers/Installing_Packager_Tools/).

Clone the repo and setup upstream tracking:
```
git clone git@gitlab.com:fedora/sigs/ai-ml/pytorch.git
cd pytorch
git remote add upstream https://github.com/pytorch/pytorch.git
git fetch --all
```

Create SRPM with [Tito](https://github.com/rpm-software-management/tito)
```
tito build --srpm --test
```

Build SRPM with [Mock](https://github.com/rpm-software-management/mock)
```
mock -r fedora-rawhide-x86_64 /path/to/pytorch-[...].src.rpm
```


### Making Changes

Edit the SPEC, add/remove patches, commit, build, test, [amend], build, test, tag, push. (or something like this)


### Building With [COPR](https://copr.fedorainfracloud.org/)

Get setup with the [COPR CLI](https://docs.pagure.org/copr.copr/user_documentation.html).

The [PyTorch COPR](https://copr.fedorainfracloud.org/coprs/g/ai-ml/pytorch/) is configured to automatically build pushed changes. To manually send a build:

```
copr-cli build @ai-ml/pytorch /path/to/pytorch-[...].src.rpm
```


#### Build Status

[![Copr build status](https://copr.fedorainfracloud.org/coprs/g/ai-ml/pytorch/package/pytorch/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/g/ai-ml/pytorch/package/pytorch/)
